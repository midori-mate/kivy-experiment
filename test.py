
from SimpleKivyApp import *

while True:
    s = input()

    if s == '1':
        print('SimpleKivyApp starts')
        # FIXME: Second run after closing the kivy window occurs the error below.
        # ctypes.ArgumentError: argument 3: <class 'TypeError'>: wrong type
        SimpleKivyApp().run()

    elif s == '2':
        print('SimpleKivyApp stops')
        # FIXME: How to stop the kivy app in program????

    elif s == '3':
        print('Exit')
        break

    else:
        print('そんなコマンドはない')
