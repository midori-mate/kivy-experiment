from kivy.app import App
from kivy.uix.widget import Widget


class Klass(Widget):

    pass


# App 名が SimpleKivyApp なので
# kv ファイル名は SimpleKivy か SimpleKivyApp になる。
class SimpleKivyApp(App):

    def build(self):
        return Klass()


if __name__ == '__main__':
    SimpleKivyApp().run()
