"""タスクトレイに常駐してホットキー押下を待ち受けるスクリプトだよ。

- wx.Frame がホットキーを待ち受ける。

それだけだとコンソール以外から App を終了する手立てがないので……

- wx.adv.TaskBarIcon から終了できるようにする。
"""

import wx
import wx.adv

# wx.App 参考ページ。
# https://wxpython.org/Phoenix/docs/html/wx.App.html
class App(wx.App):

    # なぜか wx.App ではなくて
    # wx.AppConsole に OnInit の項があるよ……。
    # https://wxpython.org/Phoenix/docs/html/wx.AppConsole.html#wx.AppConsole.OnInit
    def OnInit(self):

        # ホットキーは TaskBarIcon ではなくて
        # Frame が受け付けるみたい。
        frame = Frame()

        # SetTopWindow がどういうものかよくわからない。あってもなくてもうごく。
        # https://wxpython.org/Phoenix/docs/html/wx.App.html#wx.App.SetTopWindow
        self.SetTopWindow(frame)

        TaskBarIcon(frame)

        return True


# wx.Frame 参考ページ。
# https://wxpython.org/Phoenix/docs/html/wx.Frame.html
class Frame(wx.Frame):

    def __init__(self):

        # https://wxpython.org/Phoenix/docs/html/wx.Frame.html#api-class-api
        # この中で parent だけが必須。
        wx.Frame.__init__(
            self,
            None,
            id=wx.ID_ANY,
            title='',
            pos=wx.DefaultPosition,
            style=wx.DEFAULT_FRAME_STYLE,
        )

        # RegisterHotKey は wx.Window からの継承。
        # https://wxpython.org/Phoenix/docs/html/wx.Window.html?highlight=registerhotkey#wx.Window.RegisterHotKey
        # modifier: wx.MOD_SHIFT, wx.MOD_CONTROL, wx.MOD_ALT or wx.MOD_WIN
        # virtualKeyCode: ↓
        # https://wxpython.org/Phoenix/docs/html/wx.KeyCode.enumeration.html
        self.RegisterHotKey(1234, wx.MOD_ALT, wx.WXK_SPACE)
        self.Bind(wx.EVT_HOTKEY, self.__foo, id=1234)


    # wx.KeyEvent 参考ページ。
    # https://wxpython.org/Phoenix/docs/html/wx.KeyEvent.html
    def __foo(self, key_event):
        print('ホットキー押されたよ')


# wx.adv.TaskBarIcon 参考ページ。
# https://wxpython.org/Phoenix/docs/html/wx.adv.TaskBarIcon.html
class TaskBarIcon(wx.adv.TaskBarIcon):
    
    def __init__(self, frame):
        self.frame = frame
        super(TaskBarIcon, self).__init__()

        # アイコンを設定。
        icon = wx.Icon(wx.Bitmap('./hugging.ico'))
        self.SetIcon(icon, 'Test TaskTrayApp')

        # wx.EvtHandler.Bind 参考ページ。
        # https://wxpython.org/Phoenix/docs/html/wx.EvtHandler.html#wx.EvtHandler.Bind
        # 何らかのインスタンスメソッドを設定すること。
        # でないと、タスクトレイからアイコンが消える。なんでだよ。
        # こんなもん今回は要らないのに、イチイチ追加しないといけない。
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DCLICK, self.__foo)

    def __foo(self, event):
        print('ダブルクリックされたよ')

    # wx.Menu 参考ページ。
    # https://wxpython.org/Phoenix/docs/html/wx.Menu.html
    def CreatePopupMenu(self):
        menu = wx.Menu()
        item = wx.MenuItem(menu, wx.ID_ANY, 'Exit')
        def on_exit(event):
            wx.CallAfter(self.Destroy)
            self.frame.Close()
        menu.Bind(wx.EVT_MENU, on_exit, id=item.GetId())
        menu.Append(item)
        return menu


if __name__ == '__main__':

    # wx.App 参考ページ。
    # https://wxpython.org/Phoenix/docs/html/wx.App.html
    app = App(
        redirect=False,
        filename=None,
        useBestVisual=True,
        clearSigInt=True,
    )
    app.MainLoop()
