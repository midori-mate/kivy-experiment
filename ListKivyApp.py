from kivy.app import App
from kivy.config import Config
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty, ListProperty, ObjectProperty, BooleanProperty
from kivy.core.window import Window
import os
from pprint import pprint
from kivy.base import EventLoop
import subprocess
import platform


# ScreenManager は複数の画面を管理するためのものらしい。
# サンプルコードにあったのでそのまま使っている。
# https://pyky.github.io/kivy-doc-ja/api-kivy.uix.screenmanager.html
SM = ScreenManager(transition=NoTransition())


# kv ファイルの root。
class Manager(Screen):

    # リストを表示する RecycleView。
    recycle_view = ObjectProperty()

    # カーソル。
    cursor = 0

    # 表示するときは recycle_view.data へ入れる。
    raw_data = []


    def __init__(self, **kwargs):
        super(Manager, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)


    def on_enter(self):
        self.ids.text_input.focus = True

    def _keyboard_closed(self):
        """何のためのコードかわからない。"""
        # https://kivy.org/doc/stable/api-kivy.core.window.html
        self._keyboard = None


    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        """キー押下時イベント。"""

        self.ids.text_input.focus = True
        print(f'The key {keycode} have been pressed. Text is {text}. Modifiers are {modifiers}.')

        if keycode[1] in ['up', 'down', 'enter', 'escape']:
            self._key_event(keycode[1])


    def _key_event(self, keycode):

        # 上下カーソル。
        if keycode == 'up':
            self.cursor = self.cursor - int(self.cursor >= 1)
        elif keycode == 'down':
            self.cursor = self.cursor + int(self.cursor < len(self.raw_data)-1)

        # Enter。
        elif keycode == 'enter':
            self._on_enter()
            return True

        # Esc で終了。
        elif keycode == 'escape':
            EventLoop.exit()

        # リスト再描画。
        self._update_list()


    def _on_keyword_input(self, keyword):
        """検索語入力時イベント。"""

        # リストを更新。
        self.raw_data = self._search_prototype(keyword)

        # リスト再描画。
        self._update_list()


    def _search_prototype(self, keyword):
        """ディレクトリ検索のプロトタイプ。
        プロトタイプなんでとりあえずカレントディレクトリだけ検索だよ。
        """
        if not keyword:
            return []
        return [
            { 'text': name, 'background_color': [0.5, 0.5, 0.5, 1], }
            for name in os.listdir('./') if keyword in name
        ]


    def _update_list(self):
        """表示リストの更新。"""

        # カーソルの位置をリストに反映。
        def change_bg_default(dic):
            dic['background_color'] = [0.5, 0.5, 0.5, 1]
            return dic
        self.raw_data = list(map(change_bg_default, self.raw_data))
        if self.raw_data:
            self.raw_data[self.cursor]['background_color'] = [1, 0.5, 0.5, 1]

        # 表示。
        self.recycle_view.data = []
        self.recycle_view.data = self.raw_data


    def _on_enter(self):
        """エンター押したときのイベント。"""
        if not self.recycle_view.data:
            return
        realpath = os.path.realpath(f'./{self.recycle_view.data[self.cursor]["text"]}')

        # Win と Mac で開くときの方法が異なる……
        pf = platform.system()
        if pf == 'Windows':
            subprocess.Popen(['start', realpath], shell=True)
        elif pf == 'Darwin':
            os.system('open ' + realpath)


class Row(BoxLayout):
    text = StringProperty()
    background_color = ListProperty()


class ListKivyApp(App):

    def build(self):
        SM.add_widget(Manager(name='manager'))
        self.title = 'ListKivyApp'
        return SM


if __name__ == '__main__':
    ListKivyApp().run()
